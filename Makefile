NAME = demo
TMP  = $(NAME).unpacked

CC ?= gcc

CFLAGS  = -std=c99 -ffast-math -Os -s -fno-inline -fexpensive-optimizations -flto

ASFLAGS =
LDFLAGS = -nostdlib -lc -lm -lSDL -lGLEW  -lGL -lGLU -flto

CSRC  = demo.c
ASSRC =
OBJ   = $(CSRC:.c=.o) $(ASSRC:.S=.o)

.PHONY: all clean distclean

EXEC_SIZE = wc -c $(NAME) | cut -f1 -d' '
UNCOMP_SIZE = wc -c $(TMP) | cut -f1 -d' '

all: debug

prod: log
production: log

log: compress
	@echo "---"
	@echo "Size:                 \033[32m" `$(EXEC_SIZE)`
	@echo -n "\033[0mCompression efficency: \033[32m"
	@echo "scale=4; `$(UNCOMP_SIZE)` /  `$(EXEC_SIZE)`" | bc
	@echo -n "\033[0m"

nocolor: no-color
no-color: compress
	@echo '---'
	@echo     "Size:                 " `$(EXEC_SIZE)`
	@echo -n "Compression efficency: "
	@echo "scale=4; `$(UNCOMP_SIZE)` /  `$(EXEC_SIZE)`" | bc

compress: clean $(NAME)
	sstrip $(NAME)
	mv $(NAME) $(TMP)
	cp unpack.header $(NAME)
	gzip -cn9 $(TMP) >> $(NAME)
	chmod +x $(NAME)

debug:
	$(CC) -D DEBUG -std=c99 -g -ggdb $(LDFLAGS) $(CSRC) -o $(NAME)

$(NAME): $(OBJ)

clean:
	rm -f $(OBJ) $(TMP)

distclean: clean
	rm -f $(NAME)
