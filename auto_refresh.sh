#! /bin/sh

ls -l $1 | awk '{print $7}' > .time

while  true
do
ls -l $1 | awk '{print $7}' > .new_time
if ! cmp .new_time .time
then
  mv .new_time .time
  killall -9 demo
  ./demo&
fi
done
