#include <math.h>
#include <GL/glew.h>
#include <SDL/SDL.h>

#ifdef DEBUG
#define WIDTH 800
#define HEIGHT 600
#else
#define WIDTH 1440
#define HEIGHT 900
#endif

#ifdef DEBUG
char *loadShader(const char* filename)
{
  FILE* f = fopen(filename, "r");
  fseek(f, 0, SEEK_END);
  long len = ftell(f);
  rewind(f);
  char *res = malloc((len+1)*sizeof (char));
  fread(res, len, 1, f);
  res[len] = '\0';
  fclose(f);
  return res;
}
#endif

int t = 0;

void mixaudio(void *unused, unsigned char *stream, int len)
{
  for (int i = 0; i < len; i++, t++)
  {
    float a = t / (16+sin(t/10.0)/10.);
    stream[i] = 0*(sin((t/1000.0))*0.5+0.5) * (int)(255*sin(a)*0.5+0.5);
  }
}

GLuint createShader(const char *strShaderFile)
{
  GLuint shader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(shader, 1, &strShaderFile, NULL);

  glCompileShader(shader);

#ifdef DEBUG
  GLint status;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  if (status == GL_FALSE)
  {
    GLint infoLogLength;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);

    GLchar *strInfoLog = malloc((infoLogLength + 1) * sizeof (GLchar));
    glGetShaderInfoLog(shader, infoLogLength, NULL, strInfoLog);

    const char *strShaderType = NULL;

    fprintf(stderr, "Compile failure in fragment shader:\n%s\n", strShaderType, strInfoLog);
    free(strInfoLog);
  }
#endif

  return shader;
}

GLuint createProgram(GLuint fragmentShader)
{
  GLuint program = glCreateProgram();
  glAttachShader(program, fragmentShader);
  glLinkProgram(program);

#ifdef DEBUG
  GLint status;
  glGetProgramiv (program, GL_LINK_STATUS, &status);
  if (status == GL_FALSE)
  {
    GLint infoLogLength;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

    GLchar *strInfoLog = malloc(sizeof (GLchar) * (infoLogLength + 1));
    glGetProgramInfoLog(program, infoLogLength, NULL, strInfoLog);
    fprintf(stderr, "Linker failure: %s\n", strInfoLog);
    free(strInfoLog);
  }
#endif

  glDetachShader(program, fragmentShader);

  return program;
}

void initProgram(GLuint *program, const char *fragmentStr)
{
  GLuint fragmentShader = createShader(fragmentStr);
  *program = createProgram(fragmentShader);
  glDeleteShader(fragmentShader);
}

void _start()
{
  SDL_AudioSpec fmt =
  {
    .freq = 4096,
    .format = AUDIO_S16,
    .channels = 1,
    .samples = 512,
    .callback = mixaudio,
    .userdata = 0
  };
  SDL_OpenAudio(&fmt, NULL);

#ifdef DEBUG
  SDL_SetVideoMode(WIDTH, HEIGHT, 0, SDL_OPENGL);
#else
  SDL_SetVideoMode(WIDTH, HEIGHT, 0, SDL_OPENGL | SDL_FULLSCREEN);
#endif

  SDL_ShowCursor(SDL_DISABLE);
  glewInit();

#ifdef DEBUG
  const char *fragmentStr = loadShader("test.frag");
#else
  #include "test.frag.h"
#endif

  GLuint program;
  initProgram(&program, fragmentStr);
  GLint offsetLocation = glGetUniformLocation(program, "resolution");
  glUseProgram(program);
  glUniform2f(offsetLocation, WIDTH, HEIGHT);
  offsetLocation = glGetUniformLocation(program, "t");

  unsigned int etime, last_time, fps = 0, time = SDL_GetTicks();
  SDL_PauseAudio(0);
#ifdef DEBUG
  unsigned int last_frame = time;
  SDL_Event event;
  int done = 0;
  while (!done)
#else
    while (time/1000 < 10)
#endif
    {
      last_time = time;
      time = SDL_GetTicks();
      etime = time - last_time;
#ifdef DEBUG
      fps++;
      if (time - last_frame >= 1000)
      {
        printf("fps: %d\n", fps);
        fps = 0;
        last_frame = time;
      }
#endif


      glUniform1i(offsetLocation, t);

      glBegin(GL_QUADS);
      glVertex2f(-1.0f, -1.0f);
      glVertex2f(1.0f, -1.0f);
      glVertex2f(1.0f, 1.0f);
      glVertex2f(-1.0f, 1.0f);
      glEnd();

      SDL_GL_SwapBuffers();
#ifdef DEBUG
      SDL_PollEvent(&event);
      if (event.type == SDL_KEYDOWN)
      {
        switch (event.key.keysym.sym)
        {
          case SDLK_q:      done = 1; break;
          case SDLK_ESCAPE: done = 1; break;
        }
      }
#endif
    }

  SDL_CloseAudio();

  __asm__ (
      "movl $1,%eax\n"
      "xor %ebx,%ebx\n"
      "int $0x80\n"
      );
}
