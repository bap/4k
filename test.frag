uniform int t;
uniform vec2 resolution;
float time = float(t)/1000.0;

float ball(vec3 p, float r)
{
  return length(p) - r;
}

vec2 map(vec3 p)
{
  vec2 plane = vec2(p.y+0.95, 2.0);

  vec3 q = mod(p, vec3(2.5)) - 0.5*vec3(2.5); // repetition

  float s1 = ball(q, 0.5);
  float s2 = sin(p.x+time/5.0)*sin(p.y+time/20.0)*sin(p.z+time/10.0)*sin(time+p.x*p.z*5.)/2.;
  vec2 sphere = vec2(s1+s2/2., 1.0); // displacement

  return mix(plane, sphere, clamp(0.0, 1.0, time/20.0)); // merge
}

vec2 intersect(in vec3 ro, in vec3 rd)
{
  float t  = 0.0;
  float dt = 0.10;
  float nh = 0.0;
  float lh = 0.0;
  float lm = -1.0;

  for (int i=0; i<100; i++)
  {
    vec2 ma = map(ro+rd*t);
    nh = ma.x;
    if (nh>0.0)
      lh=nh; t+=dt;
    lm=ma.y;
  }

  if(nh > 0.0)
    return vec2(-1.0);
  t = t - dt*nh/(nh-lh);

  return vec2(t,lm);
}

float softshadow(in vec3 ro, in vec3 rd, float mint, float maxt, float k)
{
  float res = 1.0;
  float dt = 0.1;
  float t = mint;
  for( int i=0; i<30; i++ )
  {
    float h = map(ro + rd*t).x;
    if( h>0.001 )
      res = min( res, k*h/t );
    else
      res = 0.0;
    t += dt;
  }
  return res;
}

vec3 calcNormal(in vec3 p)
{
  vec2  o = vec2(.001,0.);
  vec3 n;
  n.x = map(p+o.xyy).x - map(p-o.xyy).x;
  n.y = map(p+o.yxy).x - map(p-o.yxy).x;
  n.z = map(p+o.yyx).x - map(p-o.yyx).x;
  return normalize(n);
}

void main(void)
{
  vec2 q = gl_FragCoord.xy / resolution.xy;
  vec2 p = -1.0 + 2.0 * q;
  p.x *= resolution.x/resolution.y;

  // camera
  vec3 ro = 5.*normalize(vec3(cos(0.01*time),1.0+0.4*cos(time*.011),sin(0.1*time)));
  vec3 ww = normalize(vec3(0.0,0.5,0.0) - ro);
  vec3 uu = normalize(cross( vec3(0.0,1.0,0.0), ww));
  vec3 vv = normalize(cross(ww,uu));
  vec3 rd = normalize(p.x*uu + p.y*vv + 1.5*ww);

  // raymarch
  vec3 col = vec3(0.8, 0.8, 0.8); // background color
  vec2 tmat = intersect(ro,rd);
  if(tmat.y > 0.5) // 1->ball 2->plane
  {
    // geometry
    vec3 pos = ro + tmat.x * rd;
    vec3 nor = calcNormal(pos);
    vec3 ref = reflect(rd,nor);
    vec3 lig = normalize(vec3(1.0,0.8,-0.6));

    float con = 1.0;
    float amb = 0.5 + 0.5*nor.y;
    float dif = max(dot(nor,lig),0.0);
    float bac = max(0.2 + 0.8*dot(nor,vec3(-lig.x,lig.y,-lig.z)),0.0);
    float rim = pow(1.0+dot(nor,rd),3.0);
    float spe = pow(clamp(dot(lig,ref),0.0,1.0), 16.0);

    // shadow
    float sh = softshadow( pos, lig, 0.06, 4.0, 4.0 );

    // lights
    col  = 0.10*con*vec3(1.0, 1.0, 1.0);
    col += 0.70*dif*vec3(1.0, 1.0, 1.0)*vec3(sh, (sh+sh*sh)*0.5, sh*sh );
    col += 0.20*amb*vec3(0.10,0.15,0.20);


    // color
    vec2 pro = vec2(1.0, 10.0);
    if( tmat.y<1.5 ) // ball
    {
      col *= vec3(0.0, 0.0, 0.0);
      col *= 0.5+0.5*nor.y; // fake ao
    }
    else // plane
    {
      col *= vec3(0.0, 0.0, 0.0);
      // fake ao
      col *= 1.0-smoothstep(3.0, 5.0, length(pos.xz));
      col *= smoothstep(0.0, 2.0, length(pos.xz));
    }

    // rim and spec
    col += 0.60*rim*vec3(0.0,1.0,1.0)*amb*amb; // bordure
    col += 0.60*pow(spe,pro.y)*vec3(1.0)*pro.x*sh;

    col = 0.3*col + 0.7*sqrt(col);
  }

  // vignetage
  col *= 0.25 + 0.75*pow(16.0*q.x*q.y*(1.0-q.x)*(1.0-q.y), 0.5);
  col *= 0.3 + 0.7*pow(16.0*q.x*q.y*(1.0-q.x)*(1.0-q.y), 0.3);
  col *= 0.4 + 0.6*pow(16.0*q.x*q.y*(1.0-q.x)*(1.0-q.y), 0.1*sin(time/2.));

  gl_FragColor = vec4(col,1.0);
}
